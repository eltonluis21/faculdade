#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
 * Sistema para calcular os sistemas de amortização
 * Alunos: Elton | Gustavo | Matheus | Gabriel
 * Date: 27/08/2020 21:38
 */

//Functions
float CalculateInterestPrice(float amount){
    return amount * 1.5/100;
}

float Amortization(float interest, float referenceValue){
    return referenceValue - interest;
}

float DebtBalance(float amount, float amortization){
    return amount - amortization;
}

int main()
{
    /* Variaveis */
     int option;
     float amount,percent,interest,amortization,payment,balance,rate,periods,interest_t,referenceValue,debit; 
     float i, x, v;
     i = 0;

    /* Menu do Sistema */
    printf(" SISTEMAS DE AMORTIZAÇÃO\n");
    printf("..........................\n\n");
    printf("   [  Este programa serve para calcular os sistemas de amortização    ]   \n");
    printf("   [  de um empréstimo ou financiamento, levando em consideração      ]   \n");
    printf("   [  apenas a periodicidade mais comum utilizada no mundo: a mensal. ] \n\n\n");

    printf("...::::: SELECIONE UMA OPÇÃO ABAIXO :::::...\n\n");
    printf("▒ 1.SAC ▒ 2.PRICE ▒ 3.AMERICANO ▒\n\n");

    /* Recebendo a opção escolhida pelo usuário*/
    printf("▒ ");
    scanf("%d", &option);
    printf("\n\n");

    switch (option)
    {
        case 1 :
            printf ("  [  Sistema Selecionado: SAC  ]  \n\n");

            printf("▒ Entre com o número de periodos/parcelar:\n\n▒ ");
            scanf("%f", &periods);

            printf("\n▒ Entre com a Taxa de Juros (ex: 3):\n\n▒ ");
            scanf("%f", &rate);

            printf("\n▒ Entre com o valor do empréstimo ou financiamento:\n\n▒ R$ ");
            scanf("%f", &amount);
            v = amount;

            /* Calculo do percentual de Juros*/
            percent = rate;
        
            printf("\n\n  [    Dados de entrada:    ]  \n\n");
            printf("▒ Periodos: %.0f \n▒ Taxa de Juros: %.2f \n▒ Valor: R$ %.2f \n\n\n", periods,rate,amount);
            printf("  [    TABELA    ]  \n\n");

            while (i <= periods)
            {
                if(i == 0)
                {
                    printf("▒ SALDO DEVEDOR: R$ %.2f ▒\n\n", amount);
                    i++;
                }
                else
                {
                    /* Calculando os Juros */
                    interest = (rate/100) * amount;
                    interest_t += interest;

                    /* Calculando a prestação sem os juros
                     *
                     * 1. Calculado a potencia do juros
                     * em relação a quantidade de parcelas.
                     *
                     * 2. Realizado o calculo da parcela.
                    */
                    float pow = (rate/100)+1;
                    for (x = 1; x < periods;x++)
                    {
                        pow *= (rate/100)+1;
                    }
                    payment = amount * (((pow * rate)/100) / (pow - 1));

                    /* Calculando a amortização */
                    amortization = v / periods;

                    /* Calculando o pagamento + os juros */
                    payment = amortization + interest;

                    if (i < 10)
                    {
                        printf("▒ N°:  %.0f ▒ Juros: R$ %.2f ▒ Amortização: R$ %.2f ▒ Pagamento: R$ %.2f ▒ Saldo devedor: R$ %.2f ▒\n", i,interest,amortization,payment,amount);
                    }
                    else
                    {
                        printf("▒ N°: %.0f ▒ Juros: R$ %.2f ▒ Amortização: R$ %.2f ▒ Pagamento: R$ %.2f ▒ Saldo devedor: R$ %.2f ▒\n", i,interest,amortization,payment,amount);
                    }

                    /* Recalculando o salvo devedor */
                    amount = amount - amortization;

                    i++;
                }
            }
            printf("\n▒ SALDO DEVEDOR: R$ 0.00 ▒ TOTAL DE JUROS: R$ %.2f ▒\n\n", interest_t);

        break;

        case 2 :
            // Referencia https://mundoeducacao.uol.com.br/matematica/tabela-price.htm
            printf ("  [  Sistema Selecionado: PRICE  ]  \n");

            printf("▒ Entre com o número de periodos/parcelas:\n\n▒ ");
            scanf("%f", &periods);

            printf("\n▒ Entre com a Taxa de Juros (ex: 3):\n\n▒ ");
            scanf("%f", &rate);

            printf("\n▒ Entre com o valor do empréstimo ou financiamento:\n\n▒ R$ ");
            scanf("%f", &amount);
            v = amount;

            printf("\n\n  [    Dados de entrada:    ]  \n\n");
            printf("▒ Periodos:      %.0f \n▒ Taxa de Juros: %.2f \n▒ Valor: R$ %.2f \n\n\n", periods,rate,amount);
            printf("  [    TABELA    ]  \n\n");

            //juros forma unitaria
            percent =  rate/100;

            /*  Primeiramente, temos que chegar ao valor fixo de referência para as demais parcelas.
            *   Para isso, aplica-se a fórmula
            *   Valor fixo de referência parcela = (valorFinanciamento*juros)/(1-(1/(1+2)prestações
            */
            referenceValue = amount * ((pow(percent + 1,periods) * percent) / (pow(percent + 1,periods) -1));

            while (i <= periods)
            {
                if(i == 0)
                {
                    printf("▒ SALDO DEVEDOR: R$ %.2f ▒\n\n", amount);
                    i++;
                }
                else
                {
                    //calcula o juros mensal
                    interest = CalculateInterestPrice(amount);

                    //calcula a amortização
                    amortization = Amortization(interest, referenceValue);

                    //calcula o saldo devedor
                    balance = DebtBalance(amount, amortization);

                    if (i < 10)
                    {
                        printf("▒ Mês: %.0f ▒ Juros do mês: R$ %.2f ▒ Amortização: R$ %.2f ▒ Saldo devedor: R$ %.2f ▒\n", i,interest,amortization,amount);
                    }
                    else
                    {
                        printf("▒ Mês: %.0f ▒ Juros do mês: R$ %.2f ▒ Amortização: R$ %.2f ▒ Saldo devedor: R$ %.2f ▒\n", i,interest,amortization,amount);
                    }

                    //Soma o juros total
                    interest_t += interest;

                    //atualiza o valor devedor
                    amount = amount - amortization;

                    i++;
                }
            }
            printf("\n▒ SALDO DEVEDOR: R$ 0.00 ▒ TOTAL DE JUROS: R$ %.2f ▒\n\n", interest_t);
        break;

        case 3 :
            interest_t = 0;
            printf ("  [  Sistema Selecionado: AMERICANO  ]  \n");

            printf(" Entre com o numero de periodos/parcelar:\n\n ");
            scanf("%f", &periods);

            printf("\n Entre com a Taxa de Juros (ex: 3):\n\n ");
            scanf("%f", &rate);

            printf("\n Entre com o valor do emprestimo ou financiamento:\n\n R$ ");
            scanf("%f", &amount);
            v = amount;

            percent = rate;

            printf("\n\n  [    Dados de entrada:    ]  \n\n");
            printf(" Periodos:      %.0f \n Taxa de Juros: %.2f% \n Valor:         R$ %.2f \n\n\n", periods,percent,amount);
            printf("  [    TABELA    ]  \n\n");

            /* Calculando a amortização */
            amortization = amount;

            i = 0;

            printf(" N:  %.0f  Juros: R$ 0.00  Amortizacao: R$ %.2f Saldo devedor: R$ %.2f \n", i,amortization = 0,amount);

            i = 1;

               /* Calculando os Juros */
                    interest = (rate/100) * amount;

                    interest_t += interest + amount;

            while (i <= periods - 1)
            {
                if(i == 0)
                {
                    printf(" SALDO DEVEDOR: R$ %.2f \n\n", amount);
                    i++;
                }
                else
                {
                    interest_t += interest;
                    float pow = (rate/100)+1;
                    for (x = 1; x < periods;x++)
                    {
                        pow *= (rate/100)+1;
                    }


                    if (i < 10)
                    {
                        printf(" N:  %.0f  Juros: R$ %.2f  Amortizacao: R$ %.2f Saldo devedor: R$ %.2f \n", i,interest,amortization = 0,amount);
                    }
                    else
                    {
                        printf(" N:  %.0f  Juros: R$ %.2f  Amortizacao: R$ %.2f Saldo devedor: R$ %.2f \n",  i,interest,amortization = 0,amount);
                    }

                    i++;
                }
            }
            i = periods;
            amortization = amount;
            printf(" N:  %.0f  Juros: R$ %.2f  Amortizacao: R$ %.2f Saldo devedor: R$ %.2f \n",  i,interest,amortization,amount = 0);

            printf("\n SALDO DEVEDOR: R$ 0.00  TOTAL DE JUROS: R$ %.2f \n\n", interest_t);

        break;

        default :
        printf ("Opção inválida!\n");
    }

    return 0;
}
